/** Tính tiền lương nhân viên
 * Đầu vào:
 *  Nhập vào số ngày làm workingDays = 5
 *
 *
 *
 * Các bước tiến hành:
 *  Lương = Lương 1 ngày * workingDays
 *
 *
 *
 * Đầu ra:
 *  Lương theo số ngày làm = 500000
 *
 *
 */
var salaryPerDay = 100000;
var workingDays = 5;
var result = salaryPerDay * workingDays;
console.log("Lương nhân viên: ", result);
/** Tính giá trị trung bình
 * Đầu vào:
 *  Nhập vào 5 số thực
 *      a = 1.5
 *      b = 2.5
 *      c = 3.5
 *      d = 4.5
 *      e = 5.5
 *
 *
 * Các bước tiến hành:
 *  Giá trị trung bình average = (a + b + c + d + e) / 5
 *
 *
 *
 * Đầu ra:
 *  Giá trị trung bình của 5 số thực average = 3.5
 *
 *
 */
var a = 1.5,
    b = 2.5,
    c = 3.5,
    d = 4.5,
    e = 5.5;
var average = (a + b + c + d + e) / 5;
console.log("Giá trị trung bình của 5 số là: ", average);
/** Quy đổi tiền
 * Đầu vào:
 *  Nhập vào số tiền USD cần quy đổi: 5$
 *
 *
 * Các bước tiến hành:
 *  Số tiền sau quy dổi = 23500 VND * số tiền USD cần quy đổi
 *
 *
 *
 * Đầu ra:
 *  Số tiền sau khi quy đổi: 117500
 *
 *
 */
var oneUSDtoVND = 23500;
var amountExchanged = 5;
var result = oneUSDtoVND * amountExchanged;
console.log("Số tiền sau khi quy đổi: ", result);
/** Tính diện tích, chu vi hình chữ nhật
 * Đầu vào:
 *  Nhập vào Chiều dài và Chiều rộng của hình chữ nhật
 *      Chiều dài = 10
 *      Chiều rộng = 5
 *
 *
 * Các bước tiến hành:
 *  Diện tích = Chiều dài * Chiều rộng
 *  Chu vi = (Chiều dài + Chiều rộng) * 2
 *
 *
 * Đầu ra:
 *  Diện tích Hình chữ nhật:
 *  Chu vi Hình chữ nhật:
 *
 *
 */
var length = 10;
var width = 5;
var s = length * width;
console.log("Diện tích Hình chữ nhật: ", s);
var c = (length + width) * 2;
console.log("Chu vi Hình chữ nhật: ", c);
/** Tính Tổng 2 ký số
 * Đầu vào:
 *  Nhập vào 1 số có 2 chữ số: num = 11
 *
 *
 *
 * Các bước tiến hành:
 *  Lấy số hàng đơn vị a bằng cách chia lấy dư num cho 10
 *  Sau đó lấy num chia 10 và làm tròn dưới bằng Math.floor()
 *  Tổng 2 ký số sẽ là: num + a
 *
 *
 * Đầu ra:
 *  Tổng 2 ký số vừa nhập: 2
 *
 *
 */
var num = 11;
var a = num % 10;
num = Math.floor(num / 10);
var sum = a + num;
console.log("Tổng 2 ký số vừa nhập: ", sum);
